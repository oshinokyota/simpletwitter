package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		boolean isShowMessageForm = false;
		User user = (User) request.getSession().getAttribute("loginUser");
		if(user != null) {
			isShowMessageForm = true;
		}

		String userId = request.getParameter("user_id");
		List<UserMessage> messages = new MessageService().select(userId, startDate, endDate);

		List<UserComment> comments = new CommentService().select();
		request.setAttribute("comments", comments);
		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);
		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}
